"""
1. Create your own local.py, staging.py or production.py.
2. Set environment variable correctly. (DJANGO_SETTINGS_MODULE=conf.settings.local)
"""

from django.utils.translation import ugettext_lazy as _

from .base import *

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.11/howto/static-files/

STATIC_URL = '/assets/'
STATIC_ROOT = os.path.join(BASE_DIR, 'assets/')
STATICFILES_DIRS = [
    os.path.join(BASE_DIR, 'static/'),
]

# Media files (Upload)
# https://docs.djangoproject.com/en/1.10/topics/files/

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media/')

# Internationalization
# https://docs.djangoproject.com/en/1.11/topics/i18n/

TIME_ZONE = 'Asia/Seoul'

LANGUAGE_CODE = 'ko-kr'
LANGUAGES = [
    ('en', _('English')),
    ('ko', _('Korean')),
]

# django.contrib.sites is required for disqus and other applications.
SITE_ID = 1

INSTALLED_APPS += [
    # Additional Django package
    'django.contrib.sites',

    # Third-party apps
    'tagging.apps.TaggingConfig',
    'disqus',
    'simplemde',

    # Project own apps
    'blog.apps.BlogConfig',
]

SIMPLEMDE_OPTIONS = {
    'autosave': {
        'enabled': True
    },
    'spellChecker': False,
}
