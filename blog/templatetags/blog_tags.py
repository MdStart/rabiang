from django import template
from tagging.models import TaggedItem

from ..models import Post

register = template.Library()


@register.simple_tag
def recent_posts(count=5):
    posts = Post.objects.filter(status='published').order_by('-published')[:count]
    return posts


@register.simple_tag
def simiar_posts(post, count=4):
    posts = TaggedItem.objects.get_related(post, Post, num=count)
    return posts
