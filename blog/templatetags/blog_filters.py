import bleach
import markdown
from django import template
from django.template.defaultfilters import stringfilter
from django.utils.safestring import mark_safe

register = template.Library()


@register.filter
@stringfilter
def markdownify(text):
    tags = ['h1', 'h2', 'h3', 'h4', 'h5', 'ol', 'ul', 'li', 'div', 'p', 'code', 'blockquote', 'pre',
            'table', 'thead', 'tbody', 'tr', 'th', 'td', 'a', 'em', 'strong', 'hr', 'img']

    attrs = {
        '*': ['class', 'id'],
        'a': ['href', 'rel'],
        'img': ['alt', 'src'],
    }

    return mark_safe(
        bleach.clean(
            markdown.markdown(text, output_format='html',
                              extensions=['markdown.extensions.tables',
                                          'markdown.extensions.fenced_code',
                                          'markdown.extensions.codehilite',
                                          'markdown.extensions.toc', ], ),
            tags=tags, attributes=attrs, strip=True))
