from .forms import PostSearchForm


class PageableMixin(object):
    paginate_by = 10
    page_range = 10

    def get_context_data(self, **kwargs):
        context = super(PageableMixin, self).get_context_data(**kwargs)

        start_index = int((context['page_obj'].number - 1) / self.page_range) * self.page_range
        end_index = min(start_index + self.page_range, len(context['paginator'].page_range))

        context['page_range'] = context['paginator'].page_range[start_index:end_index]

        return context


class SearchFormMixin(object):
    form_class = PostSearchForm

    def get_context_data(self, **kwargs):
        context = super(SearchFormMixin, self).get_context_data(**kwargs)

        context['form'] = self.form_class

        return context
