from django.contrib import admin
from django.utils import timezone

from .models import Post, Image


class ImageInline(admin.StackedInline):
    model = Image
    extra = 1


class PostAdmin(admin.ModelAdmin):
    list_display = ('title', 'slug', 'author', 'published', 'status')
    list_filter = ('status', 'created', 'published', 'author')
    search_fields = ('title', 'content')
    prepopulated_fields = {'slug': ('title',)}
    date_hierarchy = 'published'
    ordering = ['status', '-published']
    raw_id_fields = ('author',)
    exclude = ('published',)
    inlines = [ImageInline]

    def save_model(self, request, obj, form, change):
        if obj.status == 'published' and 'status' in form.changed_data \
                and obj.published is None:
            obj.published = timezone.now()

        super().save_model(request, obj, form, change)


class ImageAdmin(admin.ModelAdmin):
    list_display = ('post', 'image', 'created')


admin.site.register(Post, PostAdmin)
admin.site.register(Image, ImageAdmin)
