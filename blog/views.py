from django.contrib.syndication.views import Feed
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.utils.translation import ugettext_lazy as _
from django.views.generic import ListView, DetailView, TemplateView
from django.views.generic.dates import ArchiveIndexView, YearArchiveView, MonthArchiveView, DayArchiveView
from tagging.views import TaggedObjectList

from .models import Post
from .viewmixins import PageableMixin, SearchFormMixin


class PostListView(PageableMixin, SearchFormMixin, ListView):
    context_object_name = 'posts'
    template_name = 'blog/post_list.html'

    def get_queryset(self):
        form = self.form_class(self.request.GET)

        queryset = Post.objects.filter(status='published')

        if form.is_valid():
            q = form.cleaned_data['q']
            queryset = queryset.filter(Q(title__icontains=q) | Q(description__icontains=q) | Q(content__icontains=q))

        queryset = queryset.order_by('-published')

        return queryset


class PostDetailView(SearchFormMixin, DetailView):
    model = Post
    context_object_name = 'post'
    template_name = 'blog/post_detail.html'


class PostArchiveIndexView(PageableMixin, SearchFormMixin, ArchiveIndexView):
    queryset = Post.objects.filter(status='published').order_by('-published')
    context_object_name = 'posts'
    template_name = 'blog/post_archive.html'
    date_field = 'updated'


class PostYearArchiveView(PageableMixin, SearchFormMixin, YearArchiveView):
    queryset = Post.objects.filter(status='published').order_by('-published')
    context_object_name = 'posts'
    template_name = 'blog/post_archive_year.html'
    date_field = 'updated'
    make_object_list = True


class PostMonthArchiveView(PageableMixin, SearchFormMixin, MonthArchiveView):
    queryset = Post.objects.filter(status='published').order_by('-published')
    context_object_name = 'posts'
    template_name = 'blog/post_archive_month.html'
    date_field = 'updated'


class PostDayArchiveView(PageableMixin, SearchFormMixin, DayArchiveView):
    queryset = Post.objects.filter(status='published').order_by('-published')
    context_object_name = 'posts'
    template_name = 'blog/post_archive_day.html'
    date_field = 'updated'


class TaggingCloudView(SearchFormMixin, TemplateView):
    template_name = 'blog/tagging_cloud.html'


class TaggingPostListView(PageableMixin, SearchFormMixin, TaggedObjectList):
    model = Post
    context_object_name = 'posts'
    template_name = 'blog/tagging_post_list.html'


class LatestPostFeed(Feed):
    title = _('Blog Feeds')
    link = '/feed'
    description = _('Blog Recent Posts')

    def items(self):
        return Post.objects.filter(status='published').order_by('-published')[:10]

    def item_title(self, item):
        return item.title

    def item_description(self, item):
        return item.description

    def item_link(self, item):
        return reverse('blog:post_detail', args=(item.pk, item.slug,))
