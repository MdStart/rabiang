# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-06-20 07:10
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0002_post_tag'),
    ]

    operations = [
        migrations.CreateModel(
            name='Image',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('image', models.ImageField(upload_to='img/%Y/%m', verbose_name='Image')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Date created')),
                ('post', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='blog_posts', to='blog.Post', verbose_name='Post')),
            ],
            options={
                'ordering': ['created'],
            },
        ),
    ]
