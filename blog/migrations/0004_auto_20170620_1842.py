# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-06-20 09:42
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0003_image'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='description',
            field=models.TextField(blank=True, verbose_name='Description'),
        ),
    ]
