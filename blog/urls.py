from django.conf.urls import url

from .views import PostListView, PostDetailView, PostArchiveIndexView, PostYearArchiveView, PostMonthArchiveView, \
    PostDayArchiveView, TaggingCloudView, TaggingPostListView, LatestPostFeed

urlpatterns = [
    url(r'^$',
        PostListView.as_view(),
        name='post_index'),

    url(r'^(?P<pk>\d+)/(?P<slug>[-\w]+)/$',
        PostDetailView.as_view(),
        name='post_detail'),

    url(r'^archive/$',
        PostArchiveIndexView.as_view(),
        name='post_archive'),
    url(r'^archive/(?P<year>\d{4})/$',
        PostYearArchiveView.as_view(),
        name='post_year_archive'),
    url(r'^archive/(?P<year>[0-9]{4})/(?P<month>[0-9]+)/$',
        PostMonthArchiveView.as_view(month_format='%m'),
        name="post_month_archive"),
    url(r'^archive/(?P<year>[0-9]{4})/(?P<month>[0-9]+)/(?P<day>[0-9]+)/$',
        PostDayArchiveView.as_view(month_format='%m'),
        name="post_day_archive"),

    url(r'^tag/$',
        TaggingCloudView.as_view(),
        name='tagging_cloud'),
    url(r'^tag/(?P<tag>[^/]+(?u))/$',
        TaggingPostListView.as_view(),
        name='tagging_post_list'),

    url(r'^feed/$',
        LatestPostFeed(),
        name='post_feed'),
]
