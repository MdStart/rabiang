from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.db import models
from django.utils.translation import ugettext_lazy as _
from simplemde.fields import SimpleMDEField
from tagging.fields import TagField


class Post(models.Model):
    STATUS_CHOICES = (
        ('draft', _('Draft')),
        ('published', _('Published')),
    )

    title = models.CharField(verbose_name=_('Title'), max_length=250)
    slug = models.SlugField(verbose_name=_('Slug'), max_length=250, unique=False, allow_unicode=True)
    author = models.ForeignKey(User, verbose_name=_('Author'), related_name='blog_posts')
    description = models.TextField(verbose_name=_('Description'), blank=True)
    content = SimpleMDEField(verbose_name=_('Content'))
    published = models.DateTimeField(verbose_name=_('Date published'), null=True)
    created = models.DateTimeField(verbose_name=_('Date created'), auto_now_add=True)
    updated = models.DateTimeField(verbose_name=_('Date updated'), auto_now=True)
    status = models.CharField(verbose_name=_('Status'), max_length=10, choices=STATUS_CHOICES, default='draft')
    tag = TagField()

    class Meta:
        verbose_name = _('post')
        verbose_name_plural = _('posts')
        ordering = ('-published',)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('blog:post_detail', args=(self.pk, self.slug,))

    def get_previous_post(self):
        previous_post = Post.objects.filter(status='published').filter(id__lt=self.id).order_by('-published').first()

        if previous_post is None:
            return None

        return reverse('blog:post_detail', args=(previous_post.pk, previous_post.slug,))

    def get_next_post(self):
        next_post = Post.objects.filter(status='published').filter(id__gt=self.id).order_by('-published').last()

        if next_post is None:
            return None

        return reverse('blog:post_detail', args=(next_post.pk, next_post.slug,))


class Image(models.Model):
    post = models.ForeignKey(Post, verbose_name=_('Post'), related_name='blog_posts')
    image = models.ImageField(verbose_name=_('Image'), upload_to='img/%Y/%m')
    created = models.DateTimeField(verbose_name=_('Date created'), auto_now_add=True)

    class Meta:
        verbose_name = _('image')
        verbose_name_plural = _('images')
        ordering = ['created']

    def __str__(self):
        return self.image.name
