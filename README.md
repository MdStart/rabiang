# Apps

Blog
* 2-cols sidebar
* tags - django-tagging
* comments - django-disqus
* categories
* WYSIWYG editor  - django-summernote
* similar posts
* markdown
* image/file upload
* search
* trackback / pingback
